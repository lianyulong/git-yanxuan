const baseUrl = "https://lyl.sunshuaihua.top/api/"
const App = getApp();
module.exports = function http(url, data = {}, method = "GET", params = "") {
	let str = '',
		strRrl = "";
	if (params) {
		for (let item in params) {
			str += (item + '=' + params[item] + '&')
		}
		strRrl = baseUrl + url + '?' + str
	} else {
		strRrl = baseUrl + url
	}
	return new Promise((reslove, reject) => {
		wx.request({
			header: {
				"X-Nideshop-Token": App.globalData.token
			},
			url: strRrl,
			data,
			method,
			fail: function(err) {
				wx.showToast({
					title: err.errMsg,
					icon: "none"
				})
			},
			success: function(res) {
				if (res.statusCode >= 200 && res.statusCode <= 300 || res.statusCode === 304) {
					if (res.data.errno === 0) {
						reslove(res.data.data)
					} else if (res.data.errno === 401) {
						wx.showToast({
							title: res.data.errmsg,
							icon: "none"
						})
						wx.navigateTo({
							url: '/pages/login/login'
						})
						reject(res)
					} else {
						wx.showToast({
							title: res.data.errmsg,
							icon: "none"
						})
						reject(res)
					}
				} else {
					wx.showToast({
						title: res.errMsg,
						icon: "none"
					})
					reject(res)
				}
			}
		})
	})
}
