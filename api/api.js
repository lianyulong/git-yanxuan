const http = require("./http.js")

module.exports = {
	//获取首页数据
	getIndex: function() {
		return http("index/index")
	},
	//获取商品总数
	getGoodSum: function() {
		return http("goods/count")
	},
	//获取专题列表
	getTopicList(params) {
		return http('topic/list', {}, 'GET', params).then(res => {
			return res
		})
	},
	//获取分类列表
	getClassList() {
		return http("catalog/index")
	},
	//获取分类每一项
	getTopicItem(data) {
		return http('catalog/current', data, 'GET').then(res => {
			return res
		})
	},
	//获取商品详情数据
	getGoodData(data) {
		return http('goods/detail', data, 'GET').then(res => {
			return res
		})
	},
	//获取全部评论
	getCommentNum(obj) {
		return http('comment/count', {}, 'GET', {
			typeId: 0,
			...obj
		}).then(res => {
			return res
		})
	},
	//获取评论数
	getCommentList(obj) {
		return http('comment/list', {}, 'GET', {
			typeId: 0,
			...obj
		}).then(res => {
			return res
		})
	},
	//获取大家都在看
	getGoodRelated(data) {
		return http('goods/related', data, 'GET').then(res => {
			return res
		})
	},
	//登陆
	loginByWeixin(data) {
		return http('auth/loginByWeixin', data, 'POST').then(res => {
			wx.showToast({
				title: "登录成功！"
			})
			return res
		})
	},
	//加入购物车
	joinCart(data) {
		return http('cart/add', data, 'POST').then(res => {
			wx.showToast({
				title: "加入购物车成功！"
			})
			return res
		})
	},
	//获取购物车数量
	getCartNum() {
		return http('cart/goodscount').then(res => {
			return res
		})
	},
	//收藏商品
	collect(data) {
		return http('collect/addordelete', data, "POST").then(res => {
			return res
		})
	},
	//购物车
	getCart() {
		return http('cart/index').then(res => {
			return res
		})
	},
	//购物车选中被选中
	cartSelect(data) {
		return http('cart/checked', data, "POST").then(res => {
			return res
		})
	},
	//删除购物车内商品
	deleteCart(data) {
		return http('cart/delete', data, "POST").then(res => {
			return res
		})
	},
	//购物车商品加减
	alterCartNum(data) {
		return http('cart/update', data, "POST").then(res => {
			return res
		})
	},
	//订单详情
	checkoutCon(data) {
		return http('cart/checkout', data, "GET").then(res => {
			return res
		})
	},
	//搜索页面数据
	seekCon() {
		return http('search/index').then(res => {
			return res
		})
	},
	//搜索
	seek(data) {
		return http('search/helper', data, "GET").then(res => {
			return res
		})
	},
	//获取搜索详情数据
	seekConList(data) {
		return http('goods/list', data, "GET").then(res => {
			return res
		})
	},
	//删除搜索历史
	removeHistory() {
		return http('search/clearhistory', {}, "POST").then(res => {
			return res
		})
	}
}
