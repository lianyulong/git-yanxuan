//app.js
App({
	onLaunch: function() {
		wx.getSystemInfo({
			success: res => {
				this.globalData.navHeight = res.statusBarHeight;
			},
			fail(err) {
				console.log(err);
			}
		})
	},
	globalData: {
		userInfo: null,
		navHeight: 0,
		token: wx.getStorageSync("token") || "",
		userInfo: wx.getStorageSync("userInfo") ? JSON.parse(wx.getStorageSync("userInfo")) : ''
	}
})
