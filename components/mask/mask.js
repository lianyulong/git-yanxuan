const App = getApp();
Component({
	properties: {
		price: Number,//价格
		url: String,//商品图片
		height: Number,//底部高度
		goodNumber: Number,//商品最大库存
		oppen: Boolean,//组件开关
		num: Number//商品数量
	},
	data: {
		
	},
	methods: {
		//关闭mask组件
		closeMaskShow() {
			this.triggerEvent('dealActionSheet',{
				oppen: false
			})
		},
		//减数量
		minus() {
			if (this.data.num > 1) {
				this.triggerEvent('minusNum',{
					num: this.data.num
				})
			} else {
				wx.showToast({
					title: '留一个嘛！',
					image: '/assets/image/icon_error.png',
					duration: 1500
				})
			}
		},
		//加数量
		add() {
			if (this.data.num < this.data.goodNumber) {
				this.setData({
					num: ++this.data.num
				});
			} else {
				wx.showToast({
					title: '库存不足！',
					image: '/assets/image/icon_error.png',
					duration: 1500
				})
			}
		},
	},
	ready() {
		this.setData({
			navH: App.globalData.navHeight + 44
		});
	}
})
