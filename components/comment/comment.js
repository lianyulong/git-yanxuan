// components/comment/comment.js
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		goodData: Object,
		m: Object,
		border: Boolean
	},

	/**
	 * 组件的初始数据
	 */
	data: {

	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		//图片预览
		imgPreview(e) {
			var arr = []
			var fromImg = this.data.goodData === null ? this.data.m.pic_list : this.data.goodData.comment.data.pic_list
			var str = null;
			if (fromImg.length > 0) {
				fromImg.map(m => {
					console.log(m)
					arr.push(m.pic_url)
				})
				str = fromImg[e.target.dataset.index].pic_url
			} else {
				str = 'https://p1.pstatp.com/large/888e000295f39cebf717'
				arr = ['https://p1.pstatp.com/large/888e000295f39cebf717']
			}
			wx.previewImage({
				current: str,
				urls: arr
			})
		},
	},
})
