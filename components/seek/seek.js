const {
	getGoodSum
} = require("../../api/api.js")
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {

	},

	/**
	 * 组件的初始数据
	 */
	data: {

	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		getGoodNum() {
			getGoodSum().then(res => {
				this.setData({
					goodNum: res.goodsCount
				});
			})
		},
	},
	ready() {
		this.getGoodNum()
	}
})
