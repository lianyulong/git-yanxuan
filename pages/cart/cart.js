const App = getApp();
const capsule = wx.getMenuButtonBoundingClientRect();
var edit_select = [];
const {
	getCart,
	cartSelect,
	deleteCart,
	alterCartNum
} = require("../../api/api.js")
Page({
	data: {
		cartshow: false,
		redact: false,
		allSelect: true,
		editSelect: {
			item: [],
			allNum: 0,
			allSele: false
		}
	},
	onLoad: function(options) {
		this.setData({
			navH: App.globalData.navHeight + 44,
			capsuleTop: capsule.top,
			capsuleWidth: capsule.width,
			capsuleHeight: capsule.height,
			capsuleRight: capsule.right,
			capsuleLeft: capsule.left,
		});
		this.getCartData()
	},
	//获取页面数据
	getCartData() {
		getCart().then(res => {
			this.setCartData(res)
		})
	},
	setCartData(res) {
		if (res.cartList.length > 0) {
			this.setData({
				cartList: res.cartList,
				cartshow: true,
				cartCon: res.cartTotal,
				"editSelect.all": res.cartTotal.checkedGoodsCount
			})
		}
		if (res.cartTotal.checkedGoodsCount < res.cartList.length) {
			this.setData({
				allSelect: false
			})
		} else {
			this.setData({
				allSelect: true
			})
		}
	},
	onShow() {
		let pages = getCurrentPages()
		if (pages.length != 0) {
			pages[pages.length - 1].getCartData();
		}
	},
	//单选/全选
	cartItemSelect(e) {
		var i = e.target.dataset.index,
			json = {};
		if (e.target.dataset.yuan) {
			var arr = [];
			this.data.cartList.map(m => {
				arr.push(m.product_id)
			});
			var str = arr.join()
			json = {
				isChecked: !this.data.allSelect ? 1 : 0,
				productIds: str
			}
		} else {
			json = {
				isChecked: this.data.cartList[i].checked === 0 ? 1 : 0,
				productIds: this.data.cartList[i].product_id
			};
		}
		cartSelect(json).then(res => {
			this.setCartData(res)
		})
	},
	//编辑/完成
	editFulfill(e) {
		if (e.target.dataset.text === "编辑") {
			var arr = [];
			for (var i = 0; i < this.data.cartList.length; i++) {
				arr.push(false)
			}
			this.setData({
				redact: true,
				"editSelect.item": arr
			})
		} else {
			this.setData({
				redact: false
			})
			this.getCartData()
		}
	},
	//编辑的全选
	editAllSelect(e) {
		var arr = [];
		if (e.target.dataset.form === "编辑全选") {
			edit_select = [];
			for (var i = 0; i < this.data.cartList.length; i++) {
				arr.push(true)
				edit_select.push("1")
			}
			this.setData({
				"editSelect.item": arr,
				"editSelect.allNum": this.data.editSelect.item.length
			})
		} else {
			edit_select = [];
			for (var i = 0; i < this.data.cartList.length; i++) {
				arr.push(false)
			}
			this.setData({
				"editSelect.item": arr,
				"editSelect.allNum": 0
			})
		}
		this.allbtnsele()
	},
	//编辑的单选
	editItemSelect(e) {
		var str = "editSelect.item[" + e.target.dataset.index + "]"
		if (e.target.dataset.form === "选择") {
			this.setData({
				[str]: true,
				"editSelect.allNum": ++this.data.editSelect.allNum
			})
			edit_select.push("1")
		} else {
			this.setData({
				[str]: false,
				"editSelect.allNum": --this.data.editSelect.allNum
			})
			edit_select.pop()
		}
		this.allbtnsele();
	},
	//判断编辑全选按钮是否选中
	allbtnsele() {
		if (this.data.editSelect.item.length === edit_select.length) {
			this.setData({
				"editSelect.allSele": true
			})
		} else {
			this.setData({
				"editSelect.allSele": false
			})
		}
	},
	//下单/删除购物车
	buyOmit(e) {
		if (e.target.dataset.text === "删除所选") {
			var arr = [];
			for (var i = 0; i < this.data.editSelect.item.length; i++) {
				if (this.data.editSelect.item[i]) {
					arr.push(this.data.cartList[i].product_id)
				}
			}
			var str = arr.join()
			deleteCart({
				productIds: str
			}).then(res => {
				this.setCartData(res)
				this.setData({
					"editSelect.item": [],
					"editSelect.allNum": 0,
					"editSelect.allSele": false
				})
			})
		} else {
			wx.navigateTo({
				url: '../indent/indent'
			})
			console.log(e.target.dataset.text)
		}
	},
	//购物车商品加减
	alterItemtNum(e) {
		var num = this.data.cartList[e.target.dataset.index].number + e.target.dataset.num;
		if (num <= 1) return
		if (num > 99) {
			wx.showToast({
				title: "已是购物车最大"
			})
			return
		}
		alterCartNum({
			goodsId: this.data.cartList[e.target.dataset.index].goods_id,
			id: this.data.cartList[e.target.dataset.index].id,
			productId: this.data.cartList[e.target.dataset.index].product_id,
			number: num
		}).then(res => {
			var str = "cartList[" + e.target.dataset.index + "].number"
			this.setData({
				[str]: num
			})
		})
	}
})
