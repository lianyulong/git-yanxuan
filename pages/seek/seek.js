const App = getApp();
const capsule = wx.getMenuButtonBoundingClientRect()
const {
	seekCon,
	seek,
	seekConList,
	removeHistory
} = require("../../api/api.js")
Page({
	data: {
		inputVal: "",
		seekListShow: false,
		fromSeekList: [],
		conListObj: {
			keyword: '',
			page: 1,
			size: 20,
			sort: "id",
			order: "",
			categoryId: 0,
		},
		conList: [],
		focus: false,
		result: false,
		filtrateSele: 0,
		classShow: false,
		filtrateItemSele: 0
	},
	onLoad: function(options) {
		this.setData({
			navH: App.globalData.navHeight + 44,
			capsuleTop: capsule.top,
			capsuleWidth: capsule.width,
			capsuleHeight: capsule.height,
			capsuleRight: capsule.right,
			capsuleLeft: capsule.left,
		});
		this.getSeekData()
	},
	goBack() {
		wx.navigateBack()
	},
	//删除搜索历史
	removeHis() {
		removeHistory()
	},
	//获取搜索页面最初数据
	getSeekData() {
		seekCon().then(res => {
			this.setData({
				seekData: res
			})
		})
	},
	//设置搜索input内value
	setInputVal(e) {
		var val = e.detail.value;
		this.setData({
			inputVal: val,
			result: false
		})
		if (this.data.inputVal !== "") {
			this.setData({
				seekListShow: true,
				"conList.data": [],

			})
			this.fromSeekCon()
		} else {
			var json = {
				keyword: '',
				page: 1,
				size: 20,
				sort: "id",
				order: "",
				categoryId: 0,
			}
			this.setData({
				seekListShow: false,
				fromSeekList: [],
				"conList.data": [],
				conListObj: json,
				focus: false,
				filtrateSele: 0,
				classShow: false,
				filtrateItemSele: 0
			})
		}
	},
	//按下回车搜索
	searchBtn() {
		if (this.data.inputVal == "") {
			this.setData({
				seekListShow: true,
				"conList.data": [],
				result: true
			})
		} else {
			this.setData({
				"conListObj.keyword": this.data.inputVal
			})
			this.fromSeekConList(this.data.conListObj)
		}
	},
	//获取搜索内容相关列表
	fromSeekCon() {
		seek({
			keyword: this.data.inputVal
		}).then(res => {
			this.setData({
				fromSeekList: res
			})
		})
	},
	//获取搜索详情数据
	fromSeekConList(obj) {
		var json = obj
		seekConList(json).then(res => {
			this.setData({
				conList: res
			})
			this.getSeekData()
			if (res.data !== []) {
				this.setData({
					result: true
				})
			} else {
				this.setData({
					result: false
				})
			}
		})
	},
	//每一个点击后搜索
	itemSeek(e) {
		this.setData({
			"conListObj.keyword": e.target.dataset.name,
			inputVal: e.target.dataset.name,
			seekListShow: true,
		})
		this.fromSeekConList(this.data.conListObj)
	},
	//小叉号还原最初状态
	restore() {
		this.setData({
			"conListObj.keyword": "",
			inputVal: "",
			seekListShow: false,
			"conList.data": [],
			result: false,
			focus: false,
			filtrateSele: 0,
			classShow: false,
			filtrateItemSele: 0
		})
	},
	//分类下的tab切换
	filtrateClassTab(e) {
		this.setData({
			filtrateSele: e.target.dataset.index,
			classShow: false,
			'conListObj.page': 1,
			'conListObj.size': 20,
			'conListObj.sort': "id",
			'conListObj.order': "",
			'conListObj.categoryId': this.data.conList.filterCategory[e.target.dataset.index].id
		})
		this.fromSeekConList(this.data.conListObj)
	},
	//点击显示分类切换
	classConShow() {
		if (this.data.classShow) {
			this.setData({
				classShow: false,
			})
		} else {
			this.setData({
				classShow: true,
				filtrateItemSele: 2
			})
		}
	},
	//综合按钮
	sortRestore() {
		this.setData({
			filtrateItemSele: 0,
			classShow: false,
			'conListObj.page': 1,
			'conListObj.size': 20,
			'conListObj.sort': "id",
			'conListObj.order': "",
			'conListObj.categoryId': 0
		})
		this.fromSeekConList(this.data.conListObj)
	},
	//价格排序
	priceSort() {
		if (this.data.filtrateItemSele !== 1) {
			this.setData({
				filtrateItemSele: 1,
				classShow: false,
				'conListObj.sort': "price",
				'conListObj.order': "asc",
				'conListObj.page': 1,
				'conListObj.size': 20,
				'conListObj.categoryId': 0
			})
		} else {
			if (this.data.conListObj.order !== "desc") {
				this.setData({
					'conListObj.order': "desc"
				})
			} else {
				this.setData({
					'conListObj.order': "asc"
				})
			}
		}
		this.fromSeekConList(this.data.conListObj)
	},
	//上拉加载更多
	loadMore() {
		if (this.data.conListObj.page <= this.data.conList.totalPages) {
			this.setData({
				'conListObj.page': ++this.data.conListObj.page
			})
			var arr = this.data.conList.data
			seekConList(this.data.conListObj).then(res => {
				arr = arr.concat(res.data);
				this.setData({
					"conList.data": arr
				})
			})
		}
	}
})
