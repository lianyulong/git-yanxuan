const App = getApp();
const capsule = wx.getMenuButtonBoundingClientRect();
const {
	checkoutCon
} = require("../../api/api.js")
Page({
	data: {
		cartshow: false,
		redact: false,
		allSelect: true,
		editSelect: {
			item: [],
			allNum: 0,
			allSele: false
		}
	},
	onLoad: function(options) {
		this.setData({
			navH: App.globalData.navHeight + 44,
			capsuleTop: capsule.top,
			capsuleWidth: capsule.width,
			capsuleHeight: capsule.height,
			capsuleRight: capsule.right,
			capsuleLeft: capsule.left,
		});
		this.getCheckoutCon()
	},
	goBack() {
		wx.navigateBack()
	},
	getCheckoutCon(){
		checkoutCon({
			addressId: 0,
			couponId: 0
		}).then(res=>{
			this.setData({
				indentData: res
			});
			// actualPrice
			console.log(res)
		})
	}
})
