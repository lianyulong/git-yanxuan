const App = getApp();
const capsule = wx.getMenuButtonBoundingClientRect()
const {
	getTopicList
} = require("../../api/api.js")
Page({
	data: {
		topicParams: {
			page: 1,
			size: 5
		},
		topicObj: {},
		topicHidden: {
			top: false,
			bottom: false
		}
	},
	onLoad: function(options) {
		this.setData({
			navH: App.globalData.navHeight + 44,
			capsuleTop: capsule.top,
			capsuleWidth: capsule.width,
			capsuleHeight: capsule.height,
			capsuleRight: capsule.right,
			capsuleLeft: capsule.left,
		});
		this.getTopicData()
	},
	getTopicData(event) {
		wx.showLoading({
			title: '加载中',
		});
		let num = event ? event.target.dataset.num : 0
		let {
			page,
			size
		} = this.data.topicParams
		getTopicList({
			page: page + num,
			size
		}).then(res => {
			this.setData({
				topicObj: res,
				'topicParams.page': page + num
			});
			if (res) {
				wx.hideLoading()
			}
		})
	},
	onReach: function() {
		let {
			page,
			size
		} = this.data.topicParams
		let {
			totalPages,
			data
		} = this.data.topicObj
		if (page < totalPages) {
			// wx.showLoading({
			// 	title: '加载中',
			// });
			this.setData({
				"topicHidden.bottom": true
			});
			getTopicList({
				page: page + 1,
				size
			}).then(res => {
				data = data.concat(res.data);
				this.setData({
					"topicObj.data": data,
					'topicParams.page': page + 1
				});
				if (res) {
					// wx.hideLoading()
					this.setData({
						"topicHidden.top": false,
						"topicHidden.bottom": false
					});
				}
			});
		}
	},
	refresh: function(event) {
		this.setData({
			"topicObj.data": [],
			'topicParams.page': 0,
			"topicHidden.top": true
		});
		this.onReach()
		console.log(11)
	},
	onReady: function() {

	},
	onShow: function() {

	},
	onHide: function() {

	},
	onUnload: function() {

	},
	onPullDownRefresh: function() {

	},
	onReachBottom: function() {

	},
	onShareAppMessage: function() {

	}
})
