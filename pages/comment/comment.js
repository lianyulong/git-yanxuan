const App = getApp();
const capsule = wx.getMenuButtonBoundingClientRect()
const {
	getCommentNum,
	getCommentList
} = require("../../api/api.js")
Page({
	data: {
		obj: {
			page: 1,
			showType: 0,
			size: 10
		},
		commentIndex: 0,
		commentData: [],
		tabshow: false
	},
	onLoad: function(options) {
		this.setData({
			navH: App.globalData.navHeight + 44,
			capsuleTop: capsule.top,
			capsuleWidth: capsule.width,
			capsuleHeight: capsule.height,
			capsuleRight: capsule.right,
			capsuleLeft: capsule.left,
			"obj.valueId": options.id
		});
		this.geComListData()
		this.geComNumData()
	},
	goBack() {
		wx.navigateBack({
			delta: 1
		})
	},
	geComNumData() {
		var json = {
			valueId: this.data.obj.valueId
		}
		getCommentNum(json).then(res => {
			this.setData({
				commentNum: res
			});
		})
	},
	geComListData() {
		if (this.data.totalPages < this.data.obj.page) return
		var json = {
			...this.data.obj
		}
		getCommentList(json).then(res => {
			var str = "commentData[" + this.data.commentData.length + "]"
			console.log(this.data.tabshow)
			if (this.data.tabshow) {
				this.setData({
					commentData: [],
					totalPages: 1,
					"obj.showType": this.data.commentIndex,
				});
			}
			this.setData({
				[str]: res.data,
				totalPages: res.totalPages,
				"obj.page": ++this.data.obj.page,
				tabshow: false
			});
		})
	},
	commentTab(e) {
		if (e.target.id != this.data.commentIndex) {
			this.setData({
				commentIndex: e.target.id,
				"obj.page": 1,
				tabshow: true
			});
			this.geComListData()
		}
	},
	onReady: function() {},
	onShow: function() {

	},
	onHide: function() {

	},
	onUnload: function() {

	},
	onPullDownRefresh: function() {

	},
	onReachBottom: function() {

	},
	onShareAppMessage: function() {

	}
})
