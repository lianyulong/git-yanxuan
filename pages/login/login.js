const App = getApp();
const capsule = wx.getMenuButtonBoundingClientRect()
const {
	loginByWeixin
} = require("../../api/api.js")
Page({
	data: {},
	onLoad: function(options) {
		this.setData({
			navH: App.globalData.navHeight + 44,
			capsuleTop: capsule.top,
			capsuleWidth: capsule.width,
			capsuleHeight: capsule.height,
			capsuleRight: capsule.right,
			capsuleLeft: capsule.left,
		});
	},
	goBack() {
		wx.navigateBack({
			delta: 1
		})
	},
	loginUser(e) {
		if (e.detail.errMsg === 'getUserInfo:fail auth deny') {
			wx.showToast({
				title: '取消登陆！',
				icon: "none"
			})
			return
		}
		if (e.detail.errMsg === 'getUserInfo:ok') {
			wx.login({
				success(obj) {
					loginByWeixin({
						code: obj.code,
						userInfo: e.detail
					}).then(res => {
						App.globalData.userInfo = res.userInfo;
						App.globalData.token = res.token;
						wx.setStorageSync('token', res.token);
						wx.setStorageSync("userInfo", JSON.stringify(res.userInfo))
						if (res.token) {
							wx.navigateBack({
								delta: 1
							})
						}
					})
				}
			})
		}
	}
})
