const App = getApp();
const capsule = wx.getMenuButtonBoundingClientRect()
var WxParse = require('../../wxParse/wxParse.js');
const {
	getGoodData,
	getGoodRelated,
	joinCart,
	getCartNum,
	collect
} = require("../../api/api.js")
Page({
	data: {
		oppen: false,
		num: 1,
		collectGood: 0
	},
	onLoad: function(options) {
		//获取导航信息
		this.setData({
			navH: App.globalData.navHeight + 44,
			capsuleTop: capsule.top,
			capsuleWidth: capsule.width,
			capsuleHeight: capsule.height,
			capsuleRight: capsule.right,
			capsuleLeft: capsule.left,
			id: options.id,
			project_id: options.project_id
		});
		this.getData()
		this.getGoodLook()
		this.getCarNum()
		//获取底部高
		var query = wx.createSelectorQuery();
		var that = this;
		query.select('#foot').boundingClientRect(function(rect) {
			that.setData({
				height: rect.height
			})
		}).exec();
	},
	onShow() {
		let pages = getCurrentPages()
		if (pages.length != 0) {
			pages[pages.length - 1].getData();
		}
	},
	//获取购物车数量
	getCarNum() {
		getCartNum().then(res => {
			this.setData({
				carNum: res.cartTotal.goodsCount
			});
		})
	},
	//减数量
	minusNum(e) {
		this.setData({
			num: --e.detail.num
		})
	},
	//绑定mask组件
	onReady: function() {
		this.maskShow = this.selectComponent("#mask")
	},
	//打开mask组件
	oppenMask() {
		this.setData({
			oppen: true
		})
	},
	//关闭mask组件
	closeMask(e) {
		this.setData({
			oppen: e.detail.oppen
		})
	},
	//获取页面详情数据
	getData() {
		getGoodData({
			id: this.data.id
		}).then(res => {
			this.setData({
				goodData: res,
				collectGood: res.userHasCollect
			});
			if (res.info.goods_desc) {
				var article = res.info.goods_desc;
				let that = this;
				WxParse.wxParse('article', 'html', article, that)
			}
		})
	},
	//返回上一页
	goBack() {
		wx.navigateBack({
			delta: 1
		})
	},
	//图片预览
	imgPreview(e) {
		var arr = []
		let fromImg = this.data.goodData.gallery
		var str = null;
		if (fromImg.length > 0) {
			fromImg.map(m => {
				arr.push(m.img_url)
			})
			str = fromImg[e.target.dataset.index].img_url
		} else {
			str = 'https://p1.pstatp.com/large/888e000295f39cebf717'
			arr = ['https://p1.pstatp.com/large/888e000295f39cebf717']
		}
		wx.previewImage({
			current: str,
			urls: arr
		})
	},
	//查看全部评论
	goComment() {
		wx.navigateTo({
			url: "../comment/comment?id=" + this.data.id + ""
		})
	},
	//获取大家都在看
	getGoodLook() {
		getGoodRelated({
			id: this.data.id
		}).then(res => {
			this.setData({
				googLook: res
			});
		})
	},
	//加入购物车
	addCart() {
		if (this.data.oppen) {
			joinCart({
				goodsId: this.data.id,
				number: this.maskShow.data.num,
				productId: this.data.goodData.productList[0].id
			}).then(res => {
				this.setData({
					carNum: res.cartTotal.goodsCount,
					num: 1,
					oppen: false
				})
			})
		} else {
			this.oppenMask()
		}
	},
	//添加删除收藏
	addCollect() {
		collect({
			typeId: 0,
			valueId: this.data.id
		}).then(res => {
			if (res.type == "delete") {
				wx.showToast({
					title: "删除收藏成功！"
				})
				this.setData({
					collectGood: 0
				})
			} else {
				wx.showToast({
					title: "收藏成功！"
				})
				this.setData({
					collectGood: 1
				})
			}
		})
	}
})
