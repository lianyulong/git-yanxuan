const App = getApp();
const capsule = wx.getMenuButtonBoundingClientRect()
const {
	getIndex,
	getGoodSum
} = require("../../api/api.js")
Page({
	data: {

	},
	onLoad: function(options) {
		this.setData({
			navH: App.globalData.navHeight + 44,
			capsuleTop: capsule.top,
			capsuleWidth: capsule.width,
			capsuleHeight: capsule.height,
			capsuleRight: capsule.right,
			capsuleLeft: capsule.left,
		});
		this.getIndexDate()
		this.getGoodNum()
	},
	//获取首页数据
	getIndexDate() {
		getIndex().then(res => {
			this.setData({
				homeData: res
			});
		})
	},
	getGoodNum() {
		getGoodSum().then(res => {
			this.setData({
				goodNum: res.goodsCount
			});
		})
	},
	onReady: function() {

	},
	onShow: function() {

	},
	onHide: function() {

	},
	onUnload: function() {

	},
	onPullDownRefresh: function() {

	},
	onReachBottom: function() {

	},
	onShareAppMessage: function() {

	}
})
