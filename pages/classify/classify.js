const App = getApp();
const capsule = wx.getMenuButtonBoundingClientRect()
const {
	getClassList,
	getTopicItem
} = require("../../api/api.js")
Page({
	data: {
		classTab: 0
	},
	onLoad: function(options) {
		this.setData({
			navH: App.globalData.navHeight + 44,
			capsuleTop: capsule.top,
			capsuleWidth: capsule.width,
			capsuleHeight: capsule.height,
			capsuleRight: capsule.right,
			capsuleLeft: capsule.left,
		});
		this.getClassData()
	},
	getClassData() {
		getClassList().then(res => {
			this.setData({
				classDate: res
			});
		})
	},
	classCut(e) {
		if (e.target.dataset.i !== this.data.classTab) {
			this.setData({
				classTab: e.target.dataset.i
			});
			var json = {
				id: this.data.classDate.categoryList[e.target.dataset.i].id
			}
			getTopicItem(json).then(res => {
				this.setData({
					"classDate.currentCategory": res.currentCategory
				});
			})
		}
	},
	onReady: function() {

	},
	onShow: function() {

	},
	onHide: function() {

	},
	onUnload: function() {

	},
	onPullDownRefresh: function() {

	},
	onReachBottom: function() {

	},
	onShareAppMessage: function() {

	}
})
